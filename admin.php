    <?php
    /**
     * Задача 6. Реализовать вход администратора с использованием
     * HTTP-авторизации для просмотра и удаления результатов.
     **/

    // Пример HTTP-аутентификации.
    // PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
    // Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
    if (empty($_SERVER['PHP_AUTH_USER']) ||
        empty($_SERVER['PHP_AUTH_PW'])) {
        header('HTTP/1.1 401 Unanthorized');
        header('WWW-Authenticate: Basic realm="For lab6"');
        print('<h1>401 Требуется авторизация</h1>');
        exit();
    }
    $user = 'u20977';
    $pass = '2531605';
    $db = new PDO('mysql:host=localhost;dbname=u20977', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    $request = "SELECT * from admin";
    $result = $db->prepare($request);
    $result->execute();
    $flag=0;
    while($data=$result->fetch()){
        if($data['login']==$_SERVER['PHP_AUTH_USER'] && password_verify($_SERVER['PHP_AUTH_PW'],$data['hash'])){
            $flag=1;
        }
    }
    if($flag==0){
        header('HTTP/1.1 401 Unanthorized');
        header('WWW-Authenticate: Basic realm="For lab6"');
        print('<h1>401 Требуется авторизация</h1>');
        exit();
    }
    print('Вы успешно авторизовались и видите защищенные паролем данные.');
    print '<div>Привет админ!</div>';
    print '<br/>';
    session_start();
    if (empty($_SESSION['token'])) {
        $_SESSION['token'] = bin2hex(random_bytes(32));
    }
    $token = $_SESSION['token'];
    echo"$token";

    // *********
    // Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
    // Реализовать просмотр и удаление всех данных.
    // *********

    ?>
    <html>
    <head>
        <meta charset="utf-8"/>
        <title>Админка для 6 лабы</title>
        <link rel="stylesheet" media="all" href="style.css"/>

    </head>
    <body>
    <form action="delete.php" method="post" accept-charset="UTF-8">
        <label>
            <input type="number" name="delete">
        </label>
        <input type="hidden" name="delete_token" <?php print "value = '$token'";?>>
        <input type="submit" style="margin-bottom : -1em"  class="buttonform" value="Удалить запись по ID">
    </form>
    <form action="change.php" method="post" accept-charset="UTF-8">
        <label>
            <input type = "hidden" name="change_token" <?php print "value = '$token'";?>>
            <input type="number" name="change">
        </label>
        <input type="submit" style="margin-bottom : -1em"  class="buttonform" value="Изменить запись по ID">
    </form>
    <?php

    $request = "SELECT * from form JOIN all_abilities on form.user_id=all_abilities.user_id
join users on all_abilities.user_id=users.user_id";
    $result = $db ->prepare($request);
    $result->execute();
    print '<table class="table">';
    print '<tr><th>ID</th><th>Имя</th><th>E-Mail</th><th>Дата рождения</th><th>Пол</th><th>Кол-во конечностей</th>
    <th>Биография</th><th>Логин</th><th>Хэш пароля</th><th>Способность</th></tr>';
    while($data = $result->fetch()){
        print '<tr><td>';
        print $data['user_id'];
        print '</td><td>';
        print strip_tags($data['fio']);
        print '</td><td>';
        print strip_tags($data['email']);
        print '</td><td>';
        print strip_tags($data['birth']);
        print '</td><td>';
        print strip_tags($data['sex']);
        print '</td><td>';
        print strip_tags($data['limb']);
        print '</td><td>';
        print strip_tags($data['about']);
        print '</td><td>';
        print $data['login'];
        print '</td><td>';
        print $data['hash'];
        print '</td><td>';
        print strip_tags($data['abil_value']);
        print '</td></tr>';
    }
    print '</table>';

    $request = "SELECT COUNT(abil_value) FROM all_abilities where abil_value='fly' group by abil_value";
    $result = $db ->prepare($request);
    $result->execute();
    $data_fl = $result->fetch()[0];
    $request = "SELECT COUNT(abil_value) FROM all_abilities where abil_value='immortality' group by abil_value";
    $result = $db ->prepare($request);
    $result->execute();
    $data_im = $result->fetch()[0];
    $request = "SELECT COUNT(abil_value) FROM all_abilities where abil_value='telepathy' group by abil_value";
    $result = $db ->prepare($request);
    $result->execute();
    $data_tp = $result->fetch()[0];
    $request = "SELECT COUNT(abil_value) FROM all_abilities where abil_value='telekinesis' group by abil_value";
    $result = $db ->prepare($request);
    $result->execute();
    $data_tl = $result->fetch()[0];
    $request = "SELECT COUNT(abil_value) FROM all_abilities where abil_value='teleportation' group by abil_value";
    $result = $db ->prepare($request);
    $result->execute();
    $data_tep = $result->fetch()[0];
    print '<h2>Статистика по сверхспособностям:</h2>';
    print '<table class="table">';
    print '<tr><th>Полет</th><th>Бессмертие</th><th>Телепатия</th><th>Телекинез</th><th>Телепортация</th></tr>';
    print '<tr><td>';
    print $data_fl;
    print '</td><td>';
    print $data_im;
    print '</td><td>';
    print $data_tp;
    print '</td><td>';
    print $data_tl;
    print '</td><td>';
    print '</td><td>';
    print $data_tep;
    print '</td></tr>';
    print '</table>';



    ?>


    </body>
