<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login']))
    // Если есть логин в сессии, то пользователь уже авторизован.
    // TODO: Сделать выход (окончание сессии вызовом session_destroy()
    //при нажатии на кнопку Выход).
    // Делаем перенаправление на форму.
{
    if(strip_tags($_COOKIE['admin'])=='1') {
        setcookie('admin','0');
        header('Location:admin.php');
    }

    session_destroy();

    header('Location: index.php');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $flag=0;
    ?>

    <html>
    <head>
        <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sign in</title>
        <link  href="style-form.css" rel="stylesheet"  media="all"/>
    </head>
    <div class="form-container">
        <body>
        <?php
        if (!empty($messages)) {
            print('<div id="messages">');
            // Выводим все сообщения.
            foreach ($messages as $message) {
                print($message);
            }
            print('</div>');
        }
        ?>
        <div class="in-form-container">
            <form action="" accept-charset="UTF-8" method="POST">
                <div class="set">

                    <div class="formname">
                        <label>
                            <?php if ($flag==0) {print '
<a>
        <img src="https://downloader.disk.yandex.ru/preview/e48bcadbd71390e487be9c8ec2244cc7a0700079835ac6c7d4bc0656a78af1be/60884ec9/SriIYbvsnSXOQ-PYJeV7JUkA7cC7gWvxoAhZqHrzBPfxEVbdpR6ZGaqkNnGVoH5UdzO3P-HlAdu8yj3T197U7Q%3D%3D?uid=0&filename=warn.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048" 
        alt="Предупреждение"
      </a>
';} ?>Ваш логин
                            <input class="formname" type="text"  name="login" placeholder="Введите логин"
                        </label>
                    </div>
                    <div class="form_mail">
                        <label>
                            <?php if ($flag==0) {print '
<a>
        <img src="https://downloader.disk.yandex.ru/preview/e48bcadbd71390e487be9c8ec2244cc7a0700079835ac6c7d4bc0656a78af1be/60884ec9/SriIYbvsnSXOQ-PYJeV7JUkA7cC7gWvxoAhZqHrzBPfxEVbdpR6ZGaqkNnGVoH5UdzO3P-HlAdu8yj3T197U7Q%3D%3D?uid=0&filename=warn.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048" 
        alt="Предупреждение"
      </a>
';} ?>
                            Ваша пароль</label>
                        <input class="formmail" type="password" name="pass" placeholder="Введите пароль">
                    </div>
                </div>
                <input  style="color:white;margin-left: 100px" type="submit" id="send" class="buttonform" value="Отправить">
        </div>

    </div>
    </form>
    </div>
    </body>
    </div>
    </html>


    <?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
    $user = 'u20977';
    $pass = '2531605';
    $db = new PDO('mysql:host=localhost;dbname=u20977', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    $login = $db->quote($_POST['login']);
    $stmt = $db->prepare("SELECT * FROM users WHERE login LIKE ?");
    $stmt->execute([$login]);
    $flag=0;
    while($row = $stmt->fetch())
    {
        if(!strcasecmp($row['login'],$_POST['login'])&&password_verify($_POST['pass'],$row['hash']))
        {
            $flag=1;
            $user_id=$row['user_id'];
        }
    }
    if($flag) {
        // Если все ок, то авторизуем пользователя.
        $_SESSION['login'] = strip_tags($_POST['login']);
        // Записываем ID пользователя.
        $_SESSION['uid'] = $user_id;
        // Делаем перенаправление.
        header('Location: index.php');
    }
    else{
        header('Location: login.php');
    }
}

